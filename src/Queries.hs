{-# LANGUAGE OverloadedStrings #-}

module Queries where

import qualified Data.IntMap.Strict as Map
import qualified Data.Map.Strict as Map2
import Data.Maybe
import qualified Data.Set as Set
import Safe

import Model

import Database.MySQL.Simple
import Database.MySQL.Simple.QueryParams
import Database.MySQL.Simple.QueryResults
import Database.MySQL.Simple.Result

instance QueryResults Score where
    convertResults fields@[fa, fb, fc, fd, fe, ff, fg, fh, fi, fj, fk, fl, fm, fn, fo, fp, fq, fr, fs] values@[va, vb, vc, vd, ve, vf, vg, vh, vi, vj, vk, vl, vm, vn, vo, vp, vq, vr, vs] =
        fromMaybe (convertError fields values 19) maybeScore
      where
        a = convert fa va
        b = convert fb vb
        c = convert fc vc
        d = convert fd vd
        e = convert fe ve
        f = convert ff vf
        g = convert fg vg
        h = convert fh vh
        i = convert fi vi
        j = convert fj vj
        k = convert fk vk
        l = convert fl vl
        m = convert fm vm
        n = convert fn vn
        o = convert fo vo
        p = convert fp vp
        q = convert fq vq
        r = convert fr vr
        s = convert fs vs
        -- these are all necessary so that each field is inferred to the right type
        maybeScore :: Maybe Score
        maybeScore = do
            rank <- rankFromString f
            mods <- modsFromInt n
            return
                Score
                    { score_id = a
                    , Model.beatmap_id = b
                    , user_id = c
                    , score = d
                    , maxcombo = e
                    , rank = rank
                    , count50 = g
                    , count100 = h
                    , count300 = i
                    , countmiss = j
                    , countgeki = k
                    , countkatu = l
                    , perfect = m
                    , enabled_mods = mods
                    , date = o
                    , pp = p
                    , pp_old = p
                    , replay = q
                    , hidden = r
                    , country_acronym = s
                    }
    convertResults fs vs = convertError fs vs 19

scoreById :: Int -> Connection -> IO [Score]
scoreById score_id connection =
    query
        connection
        "select score_id, beatmap_id, user_id, score, maxcombo, `rank`, count50, count100, count300, countmiss, countgeki, countkatu, perfect, enabled_mods, date, pp, replay, hidden, country_acronym from osu_scores_high where score_id=?"
        (Only score_id)

scoresAbovePP :: Int -> Connection -> IO [Score]
scoresAbovePP pp connection =
    query
        connection
        "select score_id, beatmap_id, user_id, score, maxcombo, `rank`, count50, count100, count300, countmiss, countgeki, countkatu, perfect, enabled_mods, date, pp, replay, hidden, country_acronym from osu_scores_high where pp>=? order by pp desc"
        (Only pp)

scoresByMap :: Int -> Connection -> IO [Score]
scoresByMap beatmap_id connection =
    query
        connection
        "select score_id, beatmap_id, user_id, score, maxcombo, `rank`, count50, count100, count300, countmiss, countgeki, countkatu, perfect, enabled_mods, date, pp, replay, hidden, country_acronym from osu_scores_high where beatmap_id=? order by pp desc"
        (Only beatmap_id)

scoresBest :: Int -> Int -> Connection -> IO [Score]
scoresBest number user_id connection =
    query
        connection
        "select score_id, beatmap_id, user_id, score, maxcombo, `rank`, count50, count100, count300, countmiss, countgeki, countkatu, perfect, enabled_mods, date, pp, replay, hidden, country_acronym from osu_scores_high where user_id = ? order by pp desc limit ?"
        (user_id, number)

allScoresForUser :: Int -> Connection -> IO [Score]
allScoresForUser user_id connection =
    query
        connection
        "select score_id, beatmap_id, user_id, score, maxcombo, `rank`, count50, count100, count300, countmiss, countgeki, countkatu, perfect, enabled_mods, date, pp, replay, hidden, country_acronym from osu_scores_high where user_id = ? "
        (Only user_id)

allScoresForUserMaxTries :: Int -> Int -> Connection -> IO [Score]
allScoresForUserMaxTries tries user_id connection =
    query
        connection
        "select score_id, osu_scores_high.beatmap_id, osu_scores_high.user_id, score, maxcombo, `rank`, count50, count100, count300, countmiss, countgeki, countkatu, perfect, enabled_mods, date, pp, replay, hidden, country_acronym from osu_scores_high,osu_user_beatmap_playcount where osu_scores_high.user_id = ? and osu_scores_high.user_id = osu_user_beatmap_playcount.user_id and osu_user_beatmap_playcount.beatmap_id = osu_scores_high.beatmap_id and osu_user_beatmap_playcount.playcount <= ? "
        (user_id, tries)

allScoresForUserAbovePP :: Float -> Int -> Connection -> IO [Score]
allScoresForUserAbovePP pp user_id connection =
    query
        connection
        "select score_id, beatmap_id, user_id, score, maxcombo, `rank`, count50, count100, count300, countmiss, countgeki, countkatu, perfect, enabled_mods, date, pp, replay, hidden, country_acronym from osu_scores_high where pp >= ? and user_id = ? order by pp desc"
        (pp, user_id)

allScores :: Connection -> a -> (a -> Score -> IO a) -> IO a
allScores connection =
    fold_
        connection
        "select score_id, beatmap_id, user_id, score, maxcombo, `rank`, count50, count100, count300, countmiss, countgeki, countkatu, perfect, enabled_mods, date, pp, replay, hidden, country_acronym from osu_scores_high"

getScoresForUserAndRank :: Int -> String -> Connection -> IO [Score]
getScoresForUserAndRank user_id rank connection =
    query
        connection
        "select score_id, beatmap_id, user_id, score, maxcombo, `rank`, count50, count100, count300, countmiss, countgeki, countkatu, perfect, enabled_mods, date, pp, replay, hidden, country_acronym from osu_scores_high where user_id = ? and rank = ? order by pp desc"
        (user_id, rank)

instance QueryResults Player where
    convertResults fields@[fa, fb, fc] values@[va, vb, vc] =
        Player {player_id = a, player_name = b, player_pp = fromMaybe 0 c}
      where
        a = convert fa va
        b = convert fb vb
        c = convert fc vc
    convertResults fs vs = convertError fs vs 3

queryUserNamePP :: Int -> Connection -> IO (String, Maybe Float)
queryUserNamePP user_id connection = do
    [(name, pp)] <-
        query
            connection
            "select username,rank_score from osu_user_stats,sample_users where osu_user_stats.user_id = sample_users.user_id and osu_user_stats.user_id=?"
            (Only user_id)
    return (name, pp)

queryUserPP :: Int -> Connection -> IO (Maybe Float)
queryUserPP user_id connection = do
    [Only pp] <-
        query
            connection
            "select rank_score from osu_user_stats where user_id=?"
            (Only user_id)
    return pp

queryBlacklist :: Connection -> IO [Int]
queryBlacklist connection = do
    blacklist <-
        query_
            connection
            "select beatmap_id from osu_beatmap_performance_blacklist where mode = 0"
    return (fmap (\(Only id) -> id) blacklist)

allUserIds :: Connection -> IO [Int]
allUserIds connection = do
    userIds <- query_ connection "select distinct user_id from osu_scores_high"
    return (fmap (\(Only id) -> id) userIds)

topPlayers :: Int -> Connection -> IO [Player]
topPlayers topN connection = do
    players <-
        query
            connection
            "select osu_user_stats.user_id,sample_users.username,osu_user_stats.rank_score from osu_user_stats,sample_users where osu_user_stats.user_id = sample_users.user_id order by rank_score desc limit ?"
            (Only topN)
    return players

topPlayersCountry :: String -> Int -> Connection -> IO [Player]
topPlayersCountry country topN connection = do
    players <-
        query
            connection
            "select osu_user_stats.user_id,sample_users.username,osu_user_stats.rank_score from osu_user_stats,sample_users where osu_user_stats.user_id = sample_users.user_id and osu_user_stats.country_acronym = ? order by rank_score desc limit ?"
            (country, topN)
    return players

queryPlayer :: Int -> Connection -> IO (Maybe Player)
queryPlayer user_id connection = do
    players <-
        query
            connection
            "select osu_user_stats.user_id,sample_users.username,osu_user_stats.rank_score from osu_user_stats,sample_users where osu_user_stats.user_id = sample_users.user_id and osu_user_stats.user_id = ?"
            (Only user_id)
    return $ headMay players

queryPlayerAsList :: Int -> Connection -> IO [Player]
queryPlayerAsList user_id connection =
    fmap maybeToList (queryPlayer user_id connection)

queryBPM :: Int -> Connection -> IO Float
queryBPM beatmap_id connection = do
    [Only bpm] <-
        query
            connection
            "select bpm from osu_beatmaps,osu_beatmapsets where osu_beatmaps.beatmap_id = ? and osu_beatmaps.beatmapset_id = osu_beatmapsets.beatmapset_id "
            (Only beatmap_id)
    return bpm

difficultyAttributeIds :: Connection -> IO (Map.IntMap DifficultyAttribute)
difficultyAttributeIds connection = do
    xs <- query_ connection "select attrib_id,name from osu_difficulty_attribs"
    return . Map.fromList $ mapMaybe helper xs
  where
    helper (id, name) = do
        diff <- difficultyAttributeFromString name
        return (id, diff)

data DiffRow = DiffRow
    { beatmap_id :: Int
    , countNormal :: Maybe Int
    , mods :: [Mod]
    , attrib_id :: Int
    , value :: Float
    , approved :: Int
    , score_version :: Int
    , filename :: Maybe String
    , approachRate :: Float
    }

instance QueryResults DiffRow where
    convertResults fields@[fa, fb, fc, fd, fe, ff, fg, fh, fi] values@[va, vb, vc, vd, ve, vf, vg, vh, vi] =
        fromMaybe (convertError fields values 10) maybeDiffRow
      where
        a = convert fa va
        b = convert fb vb
        c = convert fc vc
        d = convert fd vd
        e = convert fe ve
        f = convert ff vf
        g = convert fg vg
        h = convert fh vh
        i = convert fi vi
        maybeDiffRow :: Maybe DiffRow
        maybeDiffRow = do
            mods <- modsFromInt c
            return $
                DiffRow
                    { Queries.beatmap_id = a
                    , countNormal = b
                    , mods = mods
                    , attrib_id = d
                    , value = e
                    , approved = f
                    , score_version = g
                    , filename = h
                    , Queries.approachRate = i
                    }
    convertResults fs vs = convertError fs vs 9

difficultyAttributeForMap ::
       Int -> Map.IntMap DifficultyAttribute -> Connection -> IO (Maybe Beatmap)
difficultyAttributeForMap beatmap_id attributeIds connection = do
    xs <-
        query
            connection
            -- "SELECT osu_beatmaps.beatmap_id,countNormal,mods,attrib_id,value,approved,score_version,osu_beatmaps.filename,osu_beatmaps.diff_approach  FROM osu_beatmaps  JOIN osu_beatmap_difficulty_attribs_old ON osu_beatmaps.beatmap_id = osu_beatmap_difficulty_attribs_old.beatmap_id  WHERE osu_beatmap_difficulty_attribs_old.mode=0 AND approved >= 1 and osu_beatmaps.beatmap_id=?"
            "SELECT osu_beatmaps.beatmap_id,countNormal,mods,attrib_id,value,approved,score_version,osu_beatmaps.filename,osu_beatmaps.diff_approach  FROM osu_beatmaps  JOIN osu_beatmap_difficulty_attribs ON osu_beatmaps.beatmap_id = osu_beatmap_difficulty_attribs.beatmap_id  WHERE osu_beatmap_difficulty_attribs.mode=0 AND approved >= 1 and osu_beatmaps.beatmap_id=?"
            (Only beatmap_id)
    return $ composeAttributeMap attributeIds xs

difficultyAttributeForAllMaps ::
       Map.IntMap DifficultyAttribute -> Connection -> IO (Map.IntMap Beatmap)
difficultyAttributeForAllMaps attributeIds connection = do
    xs <-
        query_
            connection
            -- "SELECT osu_beatmaps.beatmap_id,countNormal,mods,attrib_id,value,approved,score_version,osu_beatmaps.filename,osu_beatmaps.diff_approach  FROM osu_beatmaps  JOIN osu_beatmap_difficulty_attribs_old ON osu_beatmaps.beatmap_id = osu_beatmap_difficulty_attribs_old.beatmap_id  WHERE osu_beatmap_difficulty_attribs_old.mode=0 AND approved >= 1"
            "SELECT osu_beatmaps.beatmap_id,countNormal,mods,attrib_id,value,approved,score_version,osu_beatmaps.filename,osu_beatmaps.diff_approach  FROM osu_beatmaps  JOIN osu_beatmap_difficulty_attribs ON osu_beatmaps.beatmap_id = osu_beatmap_difficulty_attribs.beatmap_id  WHERE osu_beatmap_difficulty_attribs.mode=0 AND approved >= 1 and osu_beatmap_difficulty_attribs.attrib_id <= 9"
    return $ toBeatmapMap attributeIds $ groupById xs

difficultyAttributeForAllMapsExcudingEzHT ::
       Map.IntMap DifficultyAttribute -> Connection -> IO (Map.IntMap Beatmap)
difficultyAttributeForAllMapsExcudingEzHT attributeIds connection = do
    xs <-
        query_
            connection
            "SELECT osu_beatmaps.beatmap_id,countNormal,mods,attrib_id,value,approved,score_version,osu_beatmaps.filename,osu_beatmaps.diff_approach  FROM osu_beatmaps  JOIN osu_beatmap_difficulty_attribs ON osu_beatmaps.beatmap_id = osu_beatmap_difficulty_attribs.beatmap_id  WHERE osu_beatmap_difficulty_attribs.mode=0 AND approved >= 1 and attrib_id <= 9 AND not mods & 2 AND not mods & 256"
    return $ toBeatmapMap attributeIds $ groupById xs

difficultyAttributeForUser ::
       Int
    -> Map.IntMap DifficultyAttribute
    -> Connection
    -> IO (Map.IntMap Beatmap)
difficultyAttributeForUser userId attributeIds connection = do
    xs <-
        query
            connection
            "SELECT osu_beatmaps.beatmap_id,countNormal,mods,attrib_id,value,approved,score_version,osu_beatmaps.filename,osu_beatmaps.diff_approach  FROM osu_beatmaps  JOIN osu_beatmap_difficulty_attribs ON osu_beatmaps.beatmap_id = osu_beatmap_difficulty_attribs.beatmap_id  WHERE osu_beatmap_difficulty_attribs.mode=0 AND approved >= 1 and osu_beatmap_difficulty_attribs.attrib_id <= 9 and osu_beatmaps.beatmap_id in (select beatmap_id from osu_scores_high where user_id = ? group by beatmap_id)"
            (Only userId)
    return $ toBeatmapMap attributeIds $ groupById xs

composeAttributeMap ::
       Map.IntMap DifficultyAttribute -> [DiffRow] -> Maybe Beatmap
composeAttributeMap attributeIds xs = do
    beatmap_id <- fmap Queries.beatmap_id $ headMay xs
    scoreVersion <- fmap getScoreVersion $ headMay xs
    countCircles <- fmap getCountCircles $ headMay xs
    ranked <- fmap getRankedStatus $ headMay xs
    name <- fmap getName $ headMay xs
    approachRate <- fmap Queries.approachRate $ headMay xs
    return $
        Beatmap
            { difficultyAttributes = foldl diffFoldFunction Map2.empty xs
            , scoreVersion = scoreVersion
            , countCircles = countCircles
            , ranked = ranked
            , name = name
            , Model.approachRate = approachRate
            , beatmap__id = beatmap_id
            , bpm = -1
            }
  where
    getScoreVersion diffrow = versionFromInt (score_version diffrow)
    getCountCircles diffrow = fromMaybe 0 $ countNormal diffrow
    getRankedStatus diffrow = rankedStatusFromInt $ approved diffrow
    getName diffrow = fromMaybe "" $ filename diffrow
    diffFoldFunction ::
           DifficultyAttributesMap -> DiffRow -> DifficultyAttributesMap
    diffFoldFunction modMap diffrow =
        case Map.lookup (attrib_id diffrow) attributeIds of
            Nothing -> Map2.empty
            Just attribute ->
                Map2.insert
                    (attribute, Set.fromList (mods diffrow))
                    (value diffrow)
                    modMap

groupById :: [DiffRow] -> Map.IntMap [DiffRow]
groupById xs = foldl helper Map.empty xs
  where
    helper map diffRow =
        Map.insert
            (Queries.beatmap_id diffRow)
            (diffRow :
             (fromMaybe [] $ Map.lookup (Queries.beatmap_id diffRow) map))
            map

toBeatmapMap ::
       Map.IntMap DifficultyAttribute
    -> Map.IntMap [DiffRow]
    -> Map.IntMap Beatmap
toBeatmapMap attributeIds =
    fmap
        (fromMaybe
             (Beatmap
                  { difficultyAttributes = Map2.empty
                  , scoreVersion = ScoreV1
                  , countCircles = 0
                  , ranked = Other
                  , name = ""
                  , Model.approachRate = 0
                  , Model.beatmap__id = 0
                  , bpm = -1
                  }) .
         composeAttributeMap attributeIds)

beatmapById ::
       Int -> Map.IntMap DifficultyAttribute -> Connection -> IO (Maybe Beatmap)
beatmapById beatmap_id attributeIds connection = do
    mapAttributes <-
        difficultyAttributeForMap beatmap_id attributeIds connection
    beatmap <- sequence $ fmap (toIoBeatmap connection) mapAttributes
    return beatmap

beatmapsById ::
       Map.IntMap DifficultyAttribute -> Connection -> IO (Map.IntMap Beatmap)
beatmapsById attributeIds connection = do
    mapAttributes <- difficultyAttributeForAllMaps attributeIds connection
    beatmaps <- sequence $ fmap (toIoBeatmap connection) mapAttributes
    return beatmaps

beatmapsByIdExlcudingEzHT ::
       Map.IntMap DifficultyAttribute -> Connection -> IO (Map.IntMap Beatmap)
beatmapsByIdExlcudingEzHT attributeIds connection = do
    mapAttributes <- difficultyAttributeForAllMapsExcudingEzHT attributeIds connection
    beatmaps <- sequence $ fmap (toIoBeatmap connection) mapAttributes
    return beatmaps


allBeatmapsForUser ::
       Int -> Map.IntMap DifficultyAttribute -> Connection -> IO (Map.IntMap Beatmap)
allBeatmapsForUser userId attributeIds connection = do
    mapAttributes <- difficultyAttributeForUser userId attributeIds connection
    beatmaps <- sequence $ fmap (toIoBeatmap connection) mapAttributes
    return beatmaps

toIoBeatmap :: Connection -> Beatmap -> IO Beatmap
toIoBeatmap connection beatmap = do
    bpm <- queryBPM (Model.beatmap__id beatmap) connection
    return $ beatmap {bpm = bpm}
