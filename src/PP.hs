module PP where

import Data.List
import qualified Data.Map.Strict as Map
import qualified Data.IntMap.Strict as IntMap
import Data.Maybe
import qualified Data.Set as Set
import Model
import Debug.Trace

calcPP :: Beatmap -> Score -> Float
calcPP beatmap score = pp
  where
    (_, _, _, pp) = calcSplitPP beatmap score

calcSplitPP :: Beatmap -> Score -> (Float, Float, Float, Float)
calcSplitPP beatmap score =
    if not . null $ intersect (enabled_mods score) unrankedMods
        then (0, 0, 0, 0)
        else (aim, speed, acc, total)
  where
    mods = enabled_mods score
    aim = calcAim beatmap score
    speed = calcSpeed beatmap score
    acc = calcAcc beatmap score
    total =
        scaleMod mods NoFail 0.9 . scaleMod mods SpunOut 0.95 $
        ((aim ** 1.1 + speed ** 1.1 + acc ** 1.1) ** (1 / 1.1)) * 1.12

scaleMod :: [Mod] -> Mod -> Float -> Float -> Float
scaleMod mods mod multiplier value =
    if elem mod mods
        then value * multiplier
        else value

relevantMods :: [Mod] -> Set.Set Mod
relevantMods mods =
    Set.fromList . intersect mods $
    [DoubleTime, HalfTime, HardRock, Easy, Key4, Key5, Key6, Key7, Key8]

fromDifficultyAttributes :: Beatmap -> [Mod] -> DifficultyAttribute -> Float
fromDifficultyAttributes beatmap mods attribute =
    fromMaybe 0 $
    Map.lookup (attribute, relevantMods mods) (difficultyAttributes beatmap)


lengthBonus :: Score -> Float
lengthBonus score = 0.95 + bonus numberOfObjects
  where
    numberOfObjects = fromIntegral $ countObjects score
    bonus n 
        | n <= 2000 = 0.4 * (n / 2000)
        | n > 2000 = (bonus 2000) + (logBase 10 (n / 2000)) * 0.5

penalizeMisses :: Int -> Float -> Float
penalizeMisses countmiss value = value * (0.97 ^ countmiss)

scaleCombo :: Score -> Float -> Float -> Float
scaleCombo score mapCombo value =
    value * (min ((fromIntegral (maxcombo score) ** 0.8) / (mapCombo ** 0.8)) 1)

scaleAccuracy :: Score -> Float -> Float
scaleAccuracy score value = value * (0.5 + (getAccuracy score / 2))

scaleOD :: Float -> Float -> Float
scaleOD scoreOD value = value * (0.98 + ((scoreOD ^ 2) / 2500))

calcAim :: Beatmap -> Score -> Float
calcAim beatmap score =
    scaleOD scoreOD . scaleAccuracy score . scaleFL . scaleHD . scaleAR $
    scaleCombo score mapCombo . penalizeMisses (countmiss score) $
    applyLengthBonus $ (aimValue . touchScreenCorrection) rawAim
  where
    mods = enabled_mods score
    rawAim = fromDifficultyAttributes beatmap mods Aim
    mapCombo = fromDifficultyAttributes beatmap mods MaxCombo
    approachRate = fromDifficultyAttributes beatmap mods AR
    scoreOD = fromDifficultyAttributes beatmap mods OD
    touchScreenCorrection value =
        if elem TouchScreen mods
            then value ** 0.8
            else value
    aimValue aim = ((5 * (max 1 (aim / 0.0675)) - 4) ^ 3) / 100000
    applyLengthBonus aim = aim * lengthBonus score
    scaleAR aim = aim * (1 + factorAR)
      where
        factorAR
            | approachRate > 10.33 = 0.45 * (approachRate - 10.33)
            | approachRate < 8 && elem Hidden mods = 0.02 * (8 - approachRate)
            | approachRate < 8 = 0.01 * (8.0 - approachRate)
            | otherwise = 0
    scaleHD value =
        if elem Hidden mods
            then value * (1.02 + (11 - approachRate) / 50)
            else value
    scaleFL = scaleMod mods FlashLight (max 1 (1.45 * lengthBonus score))

calcSpeed :: Beatmap -> Score -> Float
calcSpeed beatmap score =
    scaleHD . scaleOD scoreOD . scaleAccuracy score . scaleCombo score mapCombo $
    penalizeMisses (countmiss score) . applyLengthBonus $ speedValue
  where
    mods = enabled_mods score
    scoreSpeed = fromDifficultyAttributes beatmap mods Speed
    mapCombo = fromDifficultyAttributes beatmap mods MaxCombo
    scoreOD = fromDifficultyAttributes beatmap mods OD
    speedValue = ((5 * (max 1 (scoreSpeed / 0.0675)) - 4) ^ 3) / 100000
    applyLengthBonus speed = speed * lengthBonus score
    scaleHD = scaleMod mods Hidden 1.18

calcAcc :: Beatmap -> Score -> Float
calcAcc beatmap score = scaleFL . scaleHD . applyLengthBonus . accValue $ rawAcc
  where
    mods = enabled_mods score
    numberOfCircles = countCircles beatmap
    numberOfOtherObjects = countObjects score - numberOfCircles
    accuracyV1 score
        | numberOfCircles <= 0 = 0
        | otherwise =
            max 0 $
            fromIntegral
                ((count300 score - numberOfOtherObjects) * 6 +
                 count100 score * 2 +
                 count50 score) /
            fromIntegral (numberOfCircles * 6)
    rawAcc =
        case scoreVersion beatmap of
            ScoreV2 -> getAccuracy score
            ScoreV1 -> accuracyV1 score
    scoreOD = fromDifficultyAttributes beatmap mods OD
    accValue accuracy = (1.52163 ** scoreOD) * (accuracy ^ 24) * 2.83
    applyLengthBonus acc =
        acc * (min 1.15 ((fromIntegral numberOfCircles / 1000) ** 0.3))
    scaleHD = scaleMod mods Hidden 1.02
    scaleFL = scaleMod mods FlashLight 1.02

calcUserPP :: [Int] -> [Score] -> Float
calcUserPP blacklist scores = numberBonus + diminishingPPSum 
  where
    contributingScores = sortAndFilter blacklist scores
    numberBonus = totalScoresBonus $ length contributingScores
    diminishingPPSum = dimishingSum . fmap (fromMaybe 0 . pp) $ contributingScores

calcUserPPSorted :: [Score] -> Float
calcUserPPSorted contributingScores = numberBonus + diminishingPPSum 
  where
    numberBonus = totalScoresBonus $ length contributingScores
    diminishingPPSum = dimishingSum . fmap (fromMaybe 0 . pp) $ contributingScores

sortAndFilter :: [Int] -> [Score] -> [Score]
sortAndFilter blacklist =
    sortOnPP . filterDuplicates . filterBlacklist . filterRanked
  where
    filterRanked = filter (\score -> isJust (pp score))
    filterBlacklist = filter (\score -> not $ elem (beatmap_id score) blacklist)
    filterDuplicates list =
        IntMap.elems $
        foldl
            (\map score ->
                 case IntMap.lookup (beatmap_id score) map of
                     Nothing -> IntMap.insert (beatmap_id score) score map
                     Just found ->
                         if (pp found < pp score)
                             then IntMap.insert (beatmap_id score) score map
                             else map)
            IntMap.empty
            list
    sortOnPP = sortOn (\s -> 0 - (fromMaybe 0 $ pp s))

dimishingSum :: [Float] -> Float
dimishingSum scores =
    snd $
    foldl
        (\(multiplier, sum) score ->
             (multiplier * 0.95, score * multiplier + sum))
        (1, 0)
        scores

totalScoresBonus :: Int -> Float
totalScoresBonus number = (417.0 - 1.0 / 3.0) * (1.0 - (0.9994 ^ number))
