{-# LANGUAGE OverloadedStrings #-}

module Main where

import Data.Foldable
import qualified Data.IntMap.Strict as Map
import Data.List
import Data.Maybe
import Database.MySQL.Simple
import Model
import PP
import Queries
import Text.Printf
import Utils
import Values

main :: IO ()
main = processUsers
-- main = fiddeling

fiddeling :: IO ()
fiddeling = do
    config <- readFile "Config.cfg"
    connection <- connect $ osuDatabase config
    -- blacklist <- queryBlacklist connection
    -- processUser blacklist connection 1011389
    -- processUser blacklist connection 39828
    -- processScoresAndUser sakisan connection
    processScoresAndUser 1616533  connection
    -- processScoresAndUser cookiezi connection
    -- processSpecificScores
    -- showDiffValues 1257904 connection
    return ()

shouldPrintScore :: Score -> Bool
shouldPrintScore score
 -- = fromMaybe 0 (pp score) > 500
 = False
 -- = True

processUsers :: IO ()
processUsers = do
    config <- readFile "Config.cfg"
    connection <- connect $ osuDatabase config
    blacklist <- queryBlacklist connection
    attributeIds <- difficultyAttributeIds connection
    -- userIds <- allUserIds connection
    -- players <- topPlayers 4 connection
    -- players <- topPlayers 10 connection
    -- players <- topPlayers 100 connection
    -- players <- topPlayers 950 connection
    players <- topPlayers 10000 connection
    -- players <- queryPlayerAsList 1616533 connection
    -- players <- queryPlayerAsList 731490 connection
    -- players <- topPlayersCountry "BE" 100 connection
    -- players <- topPlayers 50 connection
    -- beatmaps <- beatmapsById attributeIds connection
    beatmaps <- beatmapsByIdExlcudingEzHT attributeIds connection
    forM_ players $
        (\player ->
             processScoresAndUserWith
                 (blacklistPlus blacklist)
                 attributeIds
                 beatmaps
                 player
                 connection)
    return ()

processUser :: [Int] -> Connection -> Int -> IO ()
processUser blacklist connection id = do
    blacklist <- queryBlacklist connection
    scores <- allScoresForUser id connection
    dbValue <- queryUserPP id connection
    case dbValue of
        Nothing -> print (id)
        Just dbPP ->
            let recalcPP = calcUserPP (blacklistPlus blacklist) scores
             in if abs (recalcPP - dbPP) > 0.01
                    then print (id, dbPP, recalcPP)
                    else print (id, dbPP, "correct")

processScoresAndUser :: Int -> Connection -> IO ()
processScoresAndUser user_id connection = do
    blacklist <- queryBlacklist connection
    attributeIds <- difficultyAttributeIds connection
    maybePlayer <- queryPlayer user_id connection
    beatmaps <- allBeatmapsForUser user_id attributeIds connection
    (case maybePlayer of
         Nothing -> return ()
         Just player ->
             processScoresAndUserWith
                 (blacklistPlus blacklist)
                 attributeIds
                 beatmaps
                 player
                 connection)
    return ()

processScoresAndUserWith ::
       [Int]
    -> Map.IntMap DifficultyAttribute
    -> Map.IntMap Beatmap
    -> Player
    -> Connection
    -> IO ()
processScoresAndUserWith blacklist attributeIds beatmaps player connection = do
    -- beatmaps <- allBeatmapsForUser (player_id player) attributeIds connection
    scores <- allScoresForUser (player_id player) connection
    -- scores <- allScoresForUserMaxTries 1 (player_id player) connection
    -- scores <- allScoresForUserAbovePP 300 (player_id player) connection
    recalculatedScores <-
        fmap (writePlayerScores player) $
        fmap (sortAndFilter blacklist) $
        sequence $ -- IO [Score]
        fmap (recalculateScore attributeIds beatmaps connection) scores -- [IO Score]
    recalculatedPP <- fmap calcUserPPSorted recalculatedScores
    -- recalculatedPP <- fmap calcUserPPSorted $
    --     fmap (sortAndFilter blacklist) $
    --     sequence $ -- IO [Score]
    --     fmap (recalculateScore attributeIds beatmaps connection) scores -- [IO Score]
    putStrLn $ jsonPlayer player recalculatedPP
    return ()

recalculateScore ::
       Map.IntMap DifficultyAttribute
    -> Map.IntMap Beatmap
    -> Connection
    -> Score
    -> IO Score
recalculateScore attributeIds beatmaps connection score =
    case Map.lookup (Model.beatmap_id score) beatmaps of
        Just beatmap -> helper beatmap
        Nothing -> do
            maybeBeatmap <-
                beatmapById (Model.beatmap_id score) attributeIds connection
            fromMaybe (return score) $ fmap helper maybeBeatmap
  where
    helper :: Beatmap -> IO Score
    helper beatmap =
        if fromDifficultyAttributes beatmap (enabled_mods score) Aim > 0
            then helper2 beatmap
            else do
                maybeBeatmap <-
                    beatmapById (Model.beatmap_id score) attributeIds connection
                fromMaybe (return score) $ fmap helper2 maybeBeatmap
    helper2 beatmap =
        if shouldPrintScore score
            then do
                putStrLn $ tableScore beatmap score (newPP beatmap)
                return (newScore beatmap)
            else return (newScore beatmap)
    newScore beatmap =
        if isRanked beatmap
            then score {pp = Just $ calcPP beatmap score}
            else score {pp = Nothing}
    newPP beatmap = fromMaybe 0 $ pp (newScore beatmap)

jsonPlayer :: Player -> Float -> String
jsonPlayer player after =
    ("{\"name\":\"" ++ (player_name player)) ++
    ("\",\"id\":\"" ++ (show (player_id player))) ++
    ("\",\"before\":" ++ show (player_pp player)) ++
    ",\"after\":" ++ show after ++ "},"

tableScore :: Beatmap -> Score -> Float -> String
tableScore beatmap score after =
    printf
        "%5.2f  %5.2f  %5.2f%%  %7.2f %-40s  %s"
        after
        before
        (100 * (after - before) / before)
        (bpm beatmap)
        (show $ enabled_mods score)
        (name beatmap)
  where
    before = fromMaybe 0 $ pp score

writePlayerScores :: Player -> [Score] -> IO [Score]
writePlayerScores player scores = do
    _ <- writeFile file content
    return scores
  where
    file = "lazer/" ++ (show (player_id player)) ++ ".html"
    content =
        "<!DOCTYPE HTML><html><head><meta charset=\"UTF-8\"><title>Main</title><style>html,head,body { padding:0; margin:0; } body { font-family: calibri, helvetica, arial, sans-serif; }</style><link rel=\"stylesheet\" href=\"https://www.w3schools.com/w3css/4/w3.css\"> </head> <body> <h1>" ++
        (player_name player) ++
        "</h1> <table class=\"w3-table\"><tr><th>before</th><th>after</th><th>%change</th><th>beatmap id</th><th>accuracy</th><th>combo</th><th>mods</th></tr>" ++
        (unlines $ fmap scoreHtml $ take 300 scores) ++
        "</table> </body></html>"
    scoreHtml score =
        printf
            "<tr><td>%5.2f</td><td>%5.2f</td><td>%5.2f</td><td><a href=\"http://osu.ppy.sh/b/%i\">%i</a></td><td>%5.2f</td><td>%4i</td><td>%s</td></tr>"
            before
            after
            (100 * (after - before) / before)
            (Model.beatmap_id score)
            (Model.beatmap_id score)
            (getAccuracy score)
            (maxcombo score)
            (show (enabled_mods score))
      where
        after = (fromMaybe 0 $ pp score)
        before = (fromMaybe 0 $ pp_old score)

blacklistPlus blacklist = (1257904 : blacklist)

processSpecificScores :: IO ()
processSpecificScores = do
    config <- readFile "Config.cfg"
    connection <- connect $ osuDatabase config
    attributeIds <- difficultyAttributeIds connection
    -- scores <- allScoresForUser 345516 connection
    -- scores <- scoreById 2469554745 connection
    scores <- scoreById 2412720080 connection
    -- scores <- scoresAbovePP 700 connection
    -- scores <- scoresByMap 1256809 connection
    -- scores <- scoresByMap 114635 connection
    forM_ scores $
        (\score -> do
             beatmap <-
                 beatmapById (Model.beatmap_id score) attributeIds connection
             case beatmap of
                 Nothing ->
                     print (Model.beatmap_id score, pp score, "missing beatmap")
                 Just beatmap -> processScore beatmap score)
    return ()

processAllScores :: IO ()
processAllScores = do
    config <- readFile "Config.cfg"
    connection <- connect $ osuDatabase config
    attributeIds <- difficultyAttributeIds connection
    beatmaps <- beatmapsById attributeIds connection
    allScores
        connection
        ()
        (\_ score ->
             let beatmapLookup = Map.lookup (Model.beatmap_id score) beatmaps
              in case beatmapLookup of
                     Nothing -> return ()
                     Just beatmap -> checkScoreCorrectness beatmap score)
    return ()

processScore :: Beatmap -> Score -> IO ()
processScore beatmap score = do
    putStrLn $ tableScore beatmap score (calcPP beatmap score)
    print (calcSplitPP beatmap score)
    return ()

checkScoreCorrectness :: Beatmap -> Score -> IO ()
checkScoreCorrectness beatmap score =
    case pp score of
        Nothing
            | isRanked beatmap ->
                print (score_id score, "no pp in db", calcPP beatmap score)
            | otherwise -> return ()
        Just ppScore
            | ppScore > 0 && abs (ppScore - calcPP beatmap score) > 0.001 -> do
                print (ppScore, calcSplitPP beatmap score, name beatmap)
                print score
                putStrLn ""
            | otherwise -> return ()

printScores :: [Score] -> IO ()
printScores scores = do
    print $ length scores
    forM_
        sorted_scores
        (\score -> print (Model.beatmap_id score, enabled_mods score))
    return ()
  where
    sorted_scores = sortOn (\score -> Model.beatmap_id score) scores

showDiffValues :: Int -> Connection -> IO ()
showDiffValues id connection = do
    attributeIds <- difficultyAttributeIds connection
    beatmapResult <- beatmapById id attributeIds connection
    case beatmapResult of
        Nothing -> putStrLn "beatmap not found"
        Just beatmap -> print . difficultyAttributes $ beatmap
    return ()

osuDatabase :: String -> ConnectInfo
osuDatabase = foldl setFields defaultConnectInfo . lines
  where
    setFields info line =
        foldl
            (\result (key, setField) ->
                 if prefix key line
                     then setField $ parseProperty line
                     else result)
            info
            [ ("MySQL_db_username", (\prop -> info {connectUser = prop}))
            , ("MySQL_db_password", (\prop -> info {connectPassword = prop}))
            , ("MySQL_db_database", (\prop -> info {connectDatabase = prop}))
            , ("MySQL_db_host", (\prop -> info {connectHost = prop}))
            ]

prefix :: String -> String -> Bool
prefix [] ys = True
prefix (x:xs) [] = False
prefix (x:xs) (y:ys) = (x == y) && prefix xs ys

parseProperty :: String -> String
parseProperty = snd . foldl helper (False, [])
  where
    helper (recording, property) char =
        case (recording, char) of
            (_, '"') -> (not recording, property)
            (True, x) -> (True, property ++ [x])
            (False, _) -> (False, property)
