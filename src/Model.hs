module Model where

import Data.Bits
import qualified Data.Map.Strict as Map
import qualified Data.Set as Set
import Data.Time

data Rank
    = A
    | B
    | C
    | D
    | S
    | SH
    | X
    | XH
    deriving (Show, Eq)

data Mod
    = NoFail
    | Easy
    | TouchScreen
    | Hidden
    | HardRock
    | SuddenDeath
    | DoubleTime
    | Relax
    | HalfTime
    | NightCore
    | FlashLight
    | AutoPlay
    | SpunOut
    | Relax2
    | Perfect
    | Key4
    | Key5
    | Key6
    | Key7
    | Key8
    | FadeIn
    | Random
    | LastMod
    deriving (Show, Eq, Ord)

data Score = Score
    { score_id :: Int
    , beatmap_id :: Int
    , user_id :: Int
    , score :: Int
    , maxcombo :: Int
    , rank :: Rank
    , count50 :: Int
    , count100 :: Int
    , count300 :: Int
    , countmiss :: Int
    , countgeki :: Int
    , countkatu :: Int
    , perfect :: Int
    , enabled_mods :: [Mod]
    , date :: UTCTime
    , pp :: Maybe Float
    , pp_old :: Maybe Float
    , replay :: Int
    , hidden :: Int
    , country_acronym :: String
    } deriving (Show)

data ScoreVersion
    = ScoreV1
    | ScoreV2
    deriving (Show, Eq, Ord)

data DifficultyAttribute
    = Aim
    | Speed
    | OD
    | AR
    | MaxCombo
    | Strain
    | HitWindow300
    | ScoreMultiplier
    deriving (Show, Eq, Ord)

data RankedStatus
    = Ranked
    | Approved
    | Qualified
    | Loved
    | Other
    deriving (Show, Eq, Ord)

type DifficultyAttributesMap = Map.Map (DifficultyAttribute, Set.Set Mod) Float

data Beatmap = Beatmap
    { difficultyAttributes :: DifficultyAttributesMap
    , scoreVersion :: ScoreVersion
    , countCircles :: Int
    , ranked :: RankedStatus
    , name :: String
    , approachRate :: Float
    , bpm :: Float
    , beatmap__id :: Int
    } deriving (Show)

data Player = Player
    { player_name :: String
    , player_pp :: Float
    , player_id :: Int
    } deriving (Show)

rankedStatusFromInt :: Int -> RankedStatus
rankedStatusFromInt i =
    case i of
        1 -> Ranked
        2 -> Approved
        3 -> Qualified
        4 -> Loved
        _ -> Other

isRanked :: Beatmap -> Bool
isRanked beatmap =
    case ranked beatmap of
        Ranked -> True
        Approved -> True
        _ -> False

difficultyAttributeFromString :: String -> Maybe DifficultyAttribute
difficultyAttributeFromString string =
    case string of
        "Aim" -> Just Aim
        "Speed" -> Just Speed
        "OD" -> Just OD
        "AR" -> Just AR
        "Max combo" -> Just MaxCombo
        "Strain" -> Just Strain
        "Hit window 300" -> Just HitWindow300
        "Score multiplier" -> Just ScoreMultiplier
        _ -> Nothing

rankFromString :: String -> Maybe Rank
rankFromString f =
    case f of
        "A" -> Just A
        "B" -> Just B
        "C" -> Just C
        "D" -> Just D
        "S" -> Just S
        "SH" -> Just SH
        "X" -> Just X
        "XH" -> Just XH
        _ -> Nothing

modsFromInt :: Int -> Maybe [Mod]
modsFromInt sum =
    if sum < 0
        then Nothing
        else Just $
             pick
                 1
                 [ NoFail --   1 
                 , Easy --   2 
                 , TouchScreen --   4 
                 , Hidden --   8 
                 , HardRock --  16 
                 , SuddenDeath --  32 
                 , DoubleTime --  64 
                 , Relax -- 128 
                 , HalfTime -- 256 
                 , NightCore -- 512 
                 , FlashLight
                 , AutoPlay
                 , SpunOut
                 , Relax2
                 , Perfect
                 , Key4
                 , Key5
                 , Key6
                 , Key7
                 , Key8
                 , FadeIn
                 , Random
                 , LastMod
                 ]
  where
    pick _ [] = []
    pick i (x:xs) = pickMod i x ++ pick (i * 2) xs
    pickMod i mod =
        if sum .&. i == i
            then [mod]
            else []

unrankedMods :: [Mod]
unrankedMods = [Relax, Relax2, AutoPlay]

countObjects :: Score -> Int
countObjects score =
    (count50 score) + (count100 score) + (count300 score) + (countmiss score)

getAccuracy :: Score -> Float
getAccuracy score =
    if countObjects score == 0
        then 0
        else fromIntegral
                 ((count50 score * 50) + (count100 score * 100) +
                  (count300 score * 300)) /
             (fromIntegral (countObjects score * 300))

versionFromInt :: Int -> ScoreVersion
versionFromInt int =
    case int of
        1 -> ScoreV1
        2 -> ScoreV1
        _ -> ScoreV1

readingDensity :: Float -> Float -> Float
readingDensity ar bpm = bpm * approachTime ar / 30000

approachTime :: Float -> Float
approachTime ar =
    if ar < 5
        then 1800 - (ar * 120)
        else 1200 - ((ar - 5) * 150)

approachRateFromTime :: Float -> Float
approachRateFromTime ms =
    if ms > 1200
        then (1800 - ms) / 120
        else (1200 - ms) / 150 + 5

actualApproachRate :: Beatmap -> Score -> Float
actualApproachRate beatmap score =
    convert HalfTime (4 / 3) . convert DoubleTime (2 / 3) $
    factor Easy 0.5 . min 10 . factor HardRock 1.3 $ baseAR
  where
    baseAR = approachRate beatmap
    mods = enabled_mods score
    factor mod multiplier
        | elem mod mods = (*) multiplier
        | otherwise = id
    convert mod multiplier
        | elem mod mods = approachRateFromTime . (*) multiplier . approachTime
        | otherwise = id
        
