module Utils where

import Data.Maybe

try :: Maybe Int
try = do
    Just 5
    Nothing
    Just 6

try2 :: Int
try2 = fromMaybe 7 try
