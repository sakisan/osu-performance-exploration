"~/.ghc/ghci.conf
":def again const $ return $ unlines [":reload",":main"]
nnoremap <leader>aq :w<cr>:AsyncRun tmux send-keys -t 0 ":again" Enter<cr>
nnoremap <leader>ct :w<cr>:AsyncRun tmux send-keys -t 0 ":ctags" Enter<cr>

